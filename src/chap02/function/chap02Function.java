package chap02.function;

public class chap02Function {

	/*
	 * 
	 * -- 함수 (FUNCTION)
/* 여러개의 값 전달 ->  (SINGLE ROW)단일행 함수  -> 결과 값 여러개 */
/* 여러개의 값 전달 -> (GROUP)그룹 함수 -> 결과 값 1개*/
/*
SELECT
      * 단일행함수 + 그룹함수 둘다 올수 없다.
  FROM EMPLOYEE;    
 WHERE 그룹함수
 GROUP BY 그룹함수
 ORDER BY 그룹함수 
 
 그룹함수 : SUM, AVG, MAX, MIN, COUNT 
SELECT
       SUM(SALARY)      -- 합계
  FROM EMPLOYEE;     
 
SELECT
       AVG(SALARY)      -- 평균
  FROM EMPLOYEE; 
 
SELECT
       MIN(EMAIL)      -- 제일 작은값 ( 취급 하는 자료형 ANY TYPE (숫자o 문자도o)
     , MIN(HIRE_DATE)
     , MIN(SALARY)
  FROM EMPLOYEE; 
 
SELECT
       MAX(EMAIL)      -- 가장 큰값 ( 취급 하는 자료형 ANY TYPE (숫자o 문자도o)
     , MAX(HIRE_DATE)
     , MAX(SALARY)
  FROM EMPLOYEE; 
 
 
-- 단일함수

-- INSTR('문자열' | 컬럼명,  '문자',  찾을 위치의 시작값,  [빈도])
SELECT
       EMAIL
     , INSTR(EMAIL, '@', -1) 위치
  FROM EMPLOYEE;
  -- @ 가 몇번째 위치 ?
 
 
SELECT
       EMP_NAME
     , EMP_NO  
     , SUBSTR(EMP_NO, 1,2) 
     , SUBSTR(EMP_NO, 3,2) 
     , SUBSTR(EMP_NO, 5,2) 
  FROM EMPLOYEE;   
 
-- 입사년월일 자르기

SELECT 
       HIRE_DATE
     , SUBSTR(HIRE_DATE, 1, 2) 입사년도
     , SUBSTR(HIRE_DATE, 4, 2) 입사월
     , SUBSTR(HIRE_DATE, 7, 2) 입사일
  FROM EMPLOYEE;   

-- 여직원 구하기  
SELECT 
       EMP_NAME
     , EMP_NO
  FROM EMPLOYEE
 WHERE SUBSTR(EMP_NO, 8, 1) = '2'; 
 
-- 함수중첩
SELECT 
       EMP_NAME
     , RPAD(SUBSTR(EMP_NO,1,7) ,14, '*')
  FROM EMPLOYEE;   
 
SELECT 
       EMP_NAME
     , SUBSTR(EMAIL, 1, INSTR(EMAIL, '@') -1)
  FROM EMPLOYEE;   
 
 
-- 날짜 처리 함수 : SYSDATE, MONTHS_BETWEEN,  ADD_MONTHS, NEXT_DAY, LAST_DAY, EXTRACT
/* 
SYSDATE : 시스템에 저장되어 있는 날짜를 반환 (오늘 날짜)
MONTHS_BETWEEN (날짜, 날짜) : 두 날짜의 개월 수차이를 [숫자]로 리턴,
ADD_MONTHS (날짜, 숫자) : 날짜에 숫자만큼 개월 수를 더해서 리턴,
NEXT_DAY (기준날짜   ,  요일(문자|숫자 ) : 기준날짜에서 구하려는 요일에 가장 가까운 날짜 리턴
LAST_DAY 
EXTRACT : 년, 월, 일 정보를 추출하여 리턴,

 
SELECT SYSDATE FROM DUAL; 
SELECT
       EMP_NAME
     , HIRE_DATE
     , CEIL(MONTHS_BETWEEN(SYSDATE, HIRE_DATE))
  FROM EMPLOYEE;    
SELECT
       ADD_MONTHS(SYSDATE, 3)
       -- 오늘 날짜로 부터 3개월 뒤 출력
  FROM EMPLOYEE;  
  
SELECT
       EMP_NAME
     , HIRE_DATE  
     , ADD_MONTHS(HIRE_DATE, 6)
     -- 입사일로부터 6개월 후
FROM EMPLOYEE;

SELECT 
      *
  FROM EMPLOYEE
-- WHERE ADD_MONTHS(HIRE_DATE, 240) <= SYSDATE;
 WHERE MONTHS_BETWEEN(SYSDATE, HIRE_DATE) >= 240;
 --                 오늘날짜 , 입사일 사이가 >= 240 보다 크거나 같을때
 
-- NEXT_DAY (기준날짜,요일(문자|숫자 ) : 기준날짜에서 구하려는 요일에 가장 가까운 날짜 리턴
SELECT SYSDATE, NEXT_DAY(SYSDATE, '목요일') FROM DUAL;
SELECT SYSDATE, NEXT_DAY(SYSDATE, 5) FROM DUAL;
 
SELECT
      EMP_NAME
    , HIRE_DATE
    , LAST_DAY(HIRE_DATE) - HIRE_DATE +1 "입사월의 근무일수"
  FROM EMPLOYEE;  
  

SELECT
      EXTRACT(YEAR FROM SYSDATE)
    , EXTRACT(MONTH FROM SYSDATE)
    , EXTRACT(DAY FROM SYSDATE)
  FROM DUAL;    
 
 
 SELECT
       EMP_NAME
     , EXTRACT(YEAR FROM SYSDATE) - EXTRACT(YEAR FROM HIRE_DATE) "현재 까지 근무년수" 
  FROM EMPLOYEE;  
 
 
 SELECT
       EMP_NAME
     , HIRE_DATE
     , CEIL(MONTHS_BETWEEN(SYSDATE, HIRE_DATE) / 12) 근무년수
  FROM EMPLOYEE;
  
  
-- TO_CHAR
-- TO 들어간다 CHAR 문자로 변경
-- TO_CHAR(날짜, [포멧]) : 날짜형 데이터를 문자형 데이터로 변경한다.
-- TO_CHAR(숫자, [포멧]) : 숫자형 데이터를 문자형 데이터로 변경한다.

SELECT TO_CHAR(1234) FROM DUAL; -- 1234 문자임.
SELECT TO_CHAR(SYSDATE) FROM DUAL; -- 22/01/29
SELECT TO_CHAR(1234, '99999') FROM DUAL;  -- 99999 는 공백
SELECT TO_CHAR(1234, '00000') FROM DUAL;  -- 00000 는 빈자리 채운다.
SELECT TO_CHAR(1234, 'L99999') FROM DUAL; -- L 하면 원화 가 실행된다.
SELECT TO_CHAR(1234, '$99,999') FROM DUAL; -- $1,234
SELECT TO_CHAR(1234, '$00,000') FROM DUAL; -- $01,234

SELECT
       EMP_NAME
     , TO_CHAR(SALARY,'L9,999,999')
  FROM EMPLOYEE;   

-- 날짜 데이터 포멧 적용시에도 TO_CHAR 함수 사용
SELECT TO_CHAR(SYSDATE, 'PM HH24:MI:SS') FROM DUAL;
SELECT TO_CHAR(SYSDATE, 'AM HH:MI:SS') FROM DUAL;
SELECT TO_CHAR(SYSDATE, 'MON DY, YYYY') FROM DUAL;
SELECT TO_CHAR(SYSDATE, 'YYYY-fmMM-DD DAY') FROM DUAL;
SELECT TO_CHAR(SYSDATE, 'YEAR, Q') || '분기' FROM DUAL;

SELECT 
       EMP_NAME
     , HIRE_DATE  
     , TO_CHAR(HIRE_DATE, 'YYYY"년"-MM"월"-DD"일"') 입사일
  FROM EMPLOYEE;   


SELECT SYSDATE FROM DUAL;
 
SELECT
       TO_CHAR(SYSDATE, 'YYYY')
     , TO_CHAR(SYSDATE, 'RRRR')
     , TO_CHAR(SYSDATE, 'YY')
     , TO_CHAR(SYSDATE, 'RR')
     , TO_CHAR(SYSDATE, 'YEAR')
  FROM DUAL;     */
}